defmodule Roman do
  @roman_divisors [
    {1000, "M"},
    {900, "CM"},
    {500, "D"},
    {400, "CD"},
    {100, "C"},
    {90, "XC"},
    {50, "L"},
    {40, "XL"},
    {10, "X"},
    {9, "IX"},
    {5, "V"},
    {4, "IV"},
    {1, "I"}
  ]

  @doc """
  Convert the number to a roman number.
  """
  @spec numerals(pos_integer) :: String.t
  def numerals(number, divisors \\ @roman_divisors)
  def numerals(0, _) do "" end
  def numerals(number, [{val, sym} | t] = divisors) do
    case number >= val do
      true -> sym <> numerals(number - val, divisors)
      _ -> numerals(number, t)
    end
  end
end
