defmodule Prime do

  @doc """
  Generates the nth prime.
  """
  @spec nth(non_neg_integer) :: non_neg_integer
  def nth(0) do raise ArgumentError, message: "invalid argument 0" end
  def nth(1) do 2 end
  def nth(count) do next_prime(nth(count - 1)) end

  def next_prime(num) do
    Stream.iterate(num + 1, &(&1 + 1))
    |> Enum.find(&(prime? &1))
  end

  def prime?(1) do false end
  def prime?(2) do true end
  def prime?(x) do
    upper_bound = round(:math.sqrt(x))
    2..upper_bound
    |> Enum.all?(&(rem(x, &1) != 0))
  end
end
