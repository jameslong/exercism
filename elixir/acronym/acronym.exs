defmodule Acronym do
  @doc """
  Generate an acronym from a string.
  "This is a string" => "TIAS"
  """
  @spec abbreviate(String.t()) :: String.t()
  def abbreviate(string) do
    Regex.replace(~r/\b([a-z])/, string, fn _, x -> String.upcase(x) end)
    |> String.replace(~r/[^A-Z]/, "")
  end
end
