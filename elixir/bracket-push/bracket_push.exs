defmodule BracketPush do
  @brackets %{"]" => "[", ")" => "(", "}" => "{"}
  @opening Map.values(@brackets)
  @closing Map.keys(@brackets)

  @doc """
  Checks that all the brackets and braces in the string are matched correctly, and nested correctly
  """
  @spec check_brackets(String.t) :: boolean
  def check_brackets(str) do
    graphemes = String.graphemes(str)
    valid_brackets(graphemes, [])
  end

  def valid_brackets([], []) do: true
  def valid_brackets([], _) do: false
  def valid_brackets([h | t], unclosed) do
    case h do
      x when x in @opening -> valid_brackets(t, [x | unclosed])
      x when x in @closing ->
        match = @brackets[x]
        case unclosed do
          [y | res] when y == match -> valid_brackets(t, res)
          _ -> false
        end
      _ -> valid_brackets(t, unclosed)
    end
  end
end
