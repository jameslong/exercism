defmodule Anagram do
  @doc """
  Returns all candidates that are anagrams of, but not equal to, 'base'.
  """
  @spec match(String.t, [String.t]) :: [String.t]
  def match(base, candidates) do
    candidates
    |> Enum.filter(&anagram?(base, &1))
  end

  defp sort_chars(string) do
    string
    |> String.graphemes()
    |> Enum.sort()
  end

  defp anagram?(base, candidate) do
    base_down = String.downcase(base)
    candidate_down = String.downcase(candidate)
    base_down != candidate_down and sort_chars(base_down) == sort_chars(candidate_down)
  end
end
