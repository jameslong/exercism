defmodule ListOps do
  # Please don't use any external modules (especially List) in your
  # implementation. The point of this exercise is to create these basic functions
  # yourself.
  #
  # Note that `++` is a function from an external module (Kernel, which is
  # automatically imported) and so shouldn't be used either.

  @spec count(list) :: non_neg_integer
  def count(l) do
    l
    |> reduce(0, fn _, acc -> acc + 1 end)
  end

  @spec reverse(list) :: list
  def reverse(l) do
    l
    |> reduce([], fn x, acc -> [x | acc] end)
  end

  @spec map(list, (any -> any)) :: list
  def map(l, f) do
    l
    |> reverse()
    |> reduce([], fn x, acc -> [f.(x) | acc] end)
  end

  @spec filter(list, (any -> as_boolean(term))) :: list
  def filter(l, f) do
    l
    |> reverse()
    |> reduce([], fn x, acc ->
      if (f.(x)) do
        [x | acc]
      else
        acc
      end
    end)
  end

  @type acc :: any
  @spec reduce(list, acc, ((any, acc) -> acc)) :: acc
  def reduce([], acc, _), do: acc
  def reduce([h | t], acc, f) do
    reduce(t, f.(h, acc), f)
  end

  @spec append(list, list) :: list
  def append(l1, l2) do
    l1
    |> reverse()
    |> reduce(l2, fn x, acc -> [x | acc] end)
  end

  @spec concat([[any]]) :: [any]
  def concat(l) do
    l
    |> reduce([], &append(&2, &1))
  end
end
